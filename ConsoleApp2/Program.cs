﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public enum Country { Korea, China, Japan}

    class Program
    {
        static void Main(string[] args)
        {
            Country myCountry = Country.Korea;

            //if문
            Console.WriteLine("========== if문 ==========");
            if (myCountry == Country.Korea)
            {
                Console.WriteLine("너의 국적은 한국");
            }
            else if (myCountry == Country.China) {
                Console.WriteLine("너의 국적은 중국");
            }
            else if (myCountry == Country.Japan)
            {
                Console.WriteLine("너의 국적은 일본");
            }
            else
            {
                Console.WriteLine("너의 국적은 모르겠다.");
            }

            //switch문
            Console.WriteLine("========== switch문 ==========");
            switch (myCountry)
            {
                case Country.Korea:
                    Console.WriteLine("너의 국적은 한국");
                    break;
                case Country.China:
                    Console.WriteLine("너의 국적은 한국");
                    break;
                case Country.Japan:
                    Console.WriteLine("너의 국적은 한국");
                    break;
                default:
                    Console.WriteLine("너의 국적은 모르겠다.");
                    break;
            }

            int result = 0;
            //for문
            for (int i = 1; i <= 10; i++)
            {
                result += i;
            }
            Console.WriteLine("for문 1~10 더하기 : {0}", result);

            //foreach문
            result = 0;
            int[] arrNumber = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (int i in arrNumber)
            {
                result += i;
            }
            Console.WriteLine("foreach문 1~10 더하기 : {0}", result);

            //while문
            result = 0;
            int startNum = 1;
            int maxNum = 10;
            while (startNum <= maxNum)
            {
                result += startNum;
                startNum++;
            }
            Console.WriteLine("while문 1~10 더하기 : {0}", result);

            //do while문
            result = 0;
            startNum = 1;
            do
            {
                result += startNum;
                startNum++;
            } while (startNum <= maxNum);
            Console.WriteLine("do while문 1~10 더하기 : {0}", result);

            //break, continue문
            result = 0;
            for (int i = 1; i <= 100; i++)
            {
                if (i > 10)
                    break;

                if (i % 2 == 0)
                    continue;

                result += i;
            }
            Console.WriteLine("break-continue 1~10중 홀수만 더하기 : {0}", result);

            //goto문
            result = 0;
            for (int i = 1; i <= 10; i++)
            {
                result += i;

                if (i == 5)
                {
                    goto JUMP;
                }
            }

            //goto라벨
            JUMP:
            Console.WriteLine("goto 점프 결과 : {0}", result);

            //예외처리
            Foo("123");
            Foo(null);
            Foo("가나다");

            //1차원 배열
            int[] array1D = { 1, 2, 3, 4, 5 };
            Console.WriteLine("Rank : {0}, Length : {1}", array1D.Rank, array1D.Length);
            for (int i = 0; i < array1D.Length; i++)
            {
                Console.WriteLine("array[{0}] : {1}", i, array1D[i]);
            }

            //2차원 배열
            string[,] array2D = new string[2, 3];
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for(int j = 0; j < array2D.GetLength(1); j++)
                {
                    array2D[i, j] = String.Format("{0}-{1}", i, j);
                }
            }

            for(int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j =0; j < array2D.GetLength(1); j++)
                {
                    Console.WriteLine("{0}", array2D[i, j]);
                }
            }

            //문자열
            int number1 = 1;
            int number2 = 123;

            Console.WriteLine("number1 : |{0,5}|, number2 : {1:0000#}", number1, number2);
            Console.WriteLine("number1 : |{0,-5}|, number2 : {1:0000#}", number1, number2);

            int number3 = 12345;
            Console.WriteLine("number3 : {{ {0} }}", number3);

            int number4 = -12345;
            Console.WriteLine("number3 : {0:#,#;(#,#)}", number3);
            Console.WriteLine("number4 : {0}", number4.ToString("#,#;(#,#)"));

            //string stringBuilder 비교
            string text1 = "";
            Stopwatch sw1 = Stopwatch.StartNew();
            for (int i = 0; i < 50000; i++)
            {
                text1 += i.ToString();
            }
            sw1.Stop();

            StringBuilder text2 = new StringBuilder();
            Stopwatch sw2 = Stopwatch.StartNew();
            for (int i = 0; i < 50000; i++)
            {
                text2.Append(i.ToString());
            }
            sw2.Stop();

            Console.WriteLine("string : {0} 밀리세컨", sw1.ElapsedMilliseconds);
            Console.WriteLine("stringBuilder : {0} 밀리세컨", sw2.ElapsedMilliseconds);

            Console.Write("Press any key to continue...");
            Console.ReadKey(true);
        }

        static void Foo(String data)
        {
            try
            {
                int number = Int32.Parse(data);
                Console.WriteLine("number : {0}", number);
            }
            catch (ArgumentNullException ae)
            {
                Console.WriteLine("argumentNullException : {0}", ae.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception : {0}", ex.Message);
            }
        }
    }
}
